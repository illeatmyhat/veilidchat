# Using the Development Container
The goal is to offer a standardized development environment for ease of onboarding and sharing of configurations.

# Windows Instructions:

0. If developing, set up gitlab with SSH Keys and ssh config according to https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair
1. Set `git config --global core.autocrlf input`
2. Clone veilid/veilid and veilid/veilidchat into the same directory
3. Open `veilidchat` using VSCode. It should suggest to re-open the workspace inside the development container.
4. In the development container, run `wasm_update.sh`, `build.sh`, and `update_icons`.
5. Build APKs in `veilidchat/android` using `gradle build`. The apks will be located in `veilidchat/build/app/outputs`