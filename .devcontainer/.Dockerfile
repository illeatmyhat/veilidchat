# syntax=docker/dockerfile:1
FROM mcr.microsoft.com/devcontainers/base:bookworm

ARG CAPNPROTO_VERSION=1.0.1
ARG PROTOC_VERSION=24.3
ARG PROTOC_ARCH=x86_64
RUN echo "deb http://deb.debian.org/debian bullseye main" | sudo tee -a /etc/apt/sources.list && \
    apt update && apt install -y openjdk-17-jdk-headless clang cmake ninja-build pkg-config libgtk-3-dev liblzma-dev libstdc++-12-dev

SHELL ["/bin/bash", "-c"]
USER vscode
WORKDIR /workspaces

RUN curl -s "https://get.sdkman.io" | bash && \
    source "${HOME}/.sdkman/bin/sdkman-init.sh" && \
    sdk install gradle 7.3.3

RUN mkdir -p ~/miniconda3 && \
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh && \
    bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3 && \
    rm -rf ~/miniconda3/miniconda.sh && \
    ~/miniconda3/bin/conda init bash
ENV PATH="/home/vscode/miniconda3/bin:${PATH}"

# fails to install using the script
RUN mkdir -p ~/.local/protoc && \
    cd ~/.local/protoc && \
    curl -LO "https://github.com/protocolbuffers/protobuf/releases/download/v$PROTOC_VERSION/protoc-$PROTOC_VERSION-linux-$PROTOC_ARCH.zip" && \
    unzip "protoc-$PROTOC_VERSION-linux-$PROTOC_ARCH.zip" && \
    rm  "protoc-$PROTOC_VERSION-linux-$PROTOC_ARCH.zip"
ENV PATH="/home/vscode/.pub-cache/bin:/home/vscode/.local/protoc/bin:${PATH}"

# fails to install using the script
RUN curl -O "https://capnproto.org/capnproto-c++-$CAPNPROTO_VERSION.tar.gz" && \
    tar zxf "capnproto-c++-$CAPNPROTO_VERSION.tar.gz" && \
    cd "capnproto-c++-$CAPNPROTO_VERSION" && \
    ./configure && \
    make -j6 check && \
    sudo make install


ENV PATH="/home/vscode/flutter/bin:${PATH}"
RUN curl -OL https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.13.6-stable.tar.xz && \
    tar xf flutter_linux_3.13.6-stable.tar.xz -C ${HOME} && \
    rm flutter_linux_3.13.6-stable.tar.xz && \
    yes | flutter doctor --android-licenses
RUN --mount=type=bind,target=/workspaces/veilid/dev-setup,source=veilid/dev-setup \
    --mount=type=bind,target=/workspaces/veilid/scripts,source=veilid/scripts \
    yes | /workspaces/veilid/dev-setup/install_linux_prerequisites.sh
RUN --mount=type=bind,target=/workspaces/veilid/dev-setup,source=veilid/dev-setup \
    --mount=type=bind,target=/workspaces/veilid/scripts,source=veilid/scripts \
    --mount=type=bind,target=/workspaces/veilid/veilid-flutter,source=veilid/veilid-flutter \
    --mount=type=bind,target=/workspaces/veilidchat,source=veilidchat \
    source ~/.profile && yes | /workspaces/veilidchat/setup_linux.sh